﻿using ADA.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ADA.ViewModel
{
    public class WinStartViewModel : ViewModelBase
    {
        #region Variables privées
        #endregion

        #region Propriétés
        #endregion

        #region Propriétés de Binding    
        /// <summary>
        /// Property de binding du champs de saisie Email
        /// </summary>
        private string _mail;
        public string Mail
        {
            get { return _mail; }
            set
            {
                RaisePropertyChanged(ref _mail, value);
            }
        }
        /// <summary>
        /// Property de binding pour la valeur de l'erreur : si erreur de validation passe à true
        /// </summary>
        private bool _hasError;
        public bool HasError
        {
            get { return _hasError; }
            set
            {
                PropertyHasError(_hasError);
                RaisePropertyChanged(ref _hasError, value);
            }
        }
        /// <summary>
        /// Property de binding pour la valeur de l'erreur de la saisie du champ dédié au mail
        /// </summary>
        private bool _mailIsValid;
        public bool MailIsValid
        {
            get { return _mailIsValid; }
            set
            {
                RaisePropertyChanged(ref _mailIsValid, value);
            }
        }
        #endregion

        #region Commandes
        /// <summary>
        /// Commande du bouton de sortie de l'application
        /// </summary>
        public ICommand ExitAppCommand { get { return new RelayCommand(ExitAppMethod); } }
        /// <summary>
        /// Commande du bouton de validation général
        /// </summary>
        public ICommand OkButtonCmd { get { return new RelayCommand(OkButtonMethod); } }
        #endregion

        #region Constructeur        
        #endregion

        #region Méthodes Privées
        /// <summary>
        /// Méthode de traitement de la commande du bouton OK
        /// </summary>
        private void OkButtonMethod()
        {

        }
        /// <summary>
        /// Méthode de traitement de la validation de saisie du mail
        /// </summary>
        /// <param name="errorValue">valeure bool</param>
        private void PropertyHasError(bool errorValue)
        {
            // Implementer ici un comportement eventuellement souhaité
            if (errorValue == true)
            {
                MailIsValid = false;
            }
            else
            {
                MailIsValid = true;
            }
        }
        /// <summary>
        /// Appel de la fonction standard de fermeture de l'application identique au Form_closing
        /// </summary>
        private void ExitAppMethod()
        {
            // Appel de la fermeture standard d'un application WPF.
            System.Windows.Application.Current.MainWindow.Close();
        }
        #endregion

        #region Méthodes Publiques
        #endregion
    }
}
