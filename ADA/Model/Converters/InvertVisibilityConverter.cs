﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ADA.Model.Converters
{
    /// <summary>
    ///  Class of convertion and inversion of visibility property from ui.
    /// </summary>
    public class InvertVisibilityConverter : IValueConverter
    {
        #region IValueConverter Members
        /// <summary>
        /// First method of converter who take a visibility property and return a boolean inverse.
        /// </summary>
        public object Convert(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            if (targetType != typeof(Visibility))
                throw new InvalidOperationException("the value must be a Control Visibility WPF");

            if (!(bool)value)
            {
                return Visibility.Visible;
            }
            return Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType, object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotSupportedException();
        }

        #endregion
    }
}