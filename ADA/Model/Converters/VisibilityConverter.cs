﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;

namespace ADA.Model.Converters
{
    /// <summary>
    /// Class of convertion a booléan from visibility property from wpf ui.
    /// </summary>
    public class VisibilityConverter : IValueConverter
    {

        #region VisibilityConverter        
        public object Convert(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            bool isVisible;
            if (!(value is bool))
                throw new ArgumentException("Value must be a boolean.");
            else
                isVisible = (bool)value;

            if (parameter != null && !(bool)parameter) isVisible = !isVisible;

            if (!isVisible)
                return Visibility.Collapsed;
            else
                return Visibility.Visible;

        }


        public object ConvertBack(object value, Type targetType, object parameter, System.Globalization.CultureInfo culture)
        {
            Visibility visible;
            bool inverser = false;

            if (!(value is Visibility))
                throw new ArgumentException("Value must be a Visibility.");
            else
                visible = (Visibility)value;

            if (parameter != null && !(bool)parameter) inverser = true;

            switch (visible)
            {
                case Visibility.Collapsed:
                    return (false || inverser);
                case Visibility.Hidden:
                    return (false || inverser);
                case Visibility.Visible:
                    return (true || inverser);
                default:
                    throw new ArgumentOutOfRangeException("value", "Not Type Visibility");
            }
        }
        #endregion
    }
}