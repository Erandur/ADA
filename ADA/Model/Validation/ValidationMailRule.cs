﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace ADA.Model.Validation
{
    /// <summary>
    /// Class de validation avec traitement par règle de validation xaml pour une saisie de Mail
    /// </summary>
    public class ValidationMailRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            var s = value as string;
            if (string.IsNullOrEmpty(s))
                return new ValidationResult(false, "Le champ ne peut être vide.");
            // Déclaration et test de la saisie utilisateur dans un Pattern Regex 
            var match = Regex.Match(s, @"^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$");

            if (!match.Success)
                return new ValidationResult(false, "L'adresse mail doit être valide.");
            // Si la saisi utilisateur est valide dans sa forme et représente un mail conforme la méthode retourne true
            return new ValidationResult(true, null);
        }
    }
}
