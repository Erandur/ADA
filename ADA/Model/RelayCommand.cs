﻿using System;
using System.Windows.Input;

namespace ADA.Model
{
    // <summary>
    /// Classe de traitement des Commandes via Binding WPF.
    /// </summary>
    public sealed class RelayCommand : ICommand
    {

        public event EventHandler CanExecuteChanged;

        private readonly Action _action;
        private readonly Action<object> _actionWithParam;
        private readonly Func<bool> _canExecute;

        private bool _actualCanExecute = false;

        #region Contructeur
        public RelayCommand(Action action, Func<bool> canExecute = null)
        {
            this._action = action;
            if (canExecute != null)
            {
                this._canExecute = canExecute;
            }
            else
            {
                this._canExecute = new Func<bool>(() => true);
            }
        }
        public RelayCommand(Action<object> action, Func<bool> canExecute = null)
        {
            this._actionWithParam = action;
            if (canExecute != null)
            {
                this._canExecute = canExecute;
            }
            else
            {
                this._canExecute = new Func<bool>(() => true);
            }
        }
        #endregion

        #region Méthodes
        public bool CanExecute(object parameter)
        {
            bool ce = _canExecute();

            if (ce != _actualCanExecute)
            {
                _actualCanExecute = ce;
                if (CanExecuteChanged != null)
                {
                    CanExecuteChanged(this, new EventArgs());
                }
            }
            return ce;
        }
        public void Execute(object parameter)
        {
            if (parameter == null && _action != null)
            {
                _action();
            }
            if (parameter != null && _actionWithParam != null)
            {
                _actionWithParam(parameter);
            }
        }
        #endregion
    }
}
