﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;

namespace ADA.Model
{
    /// <summary>
    /// Classe de fonctionnement de l'application, elle gère à la fois l'évènement de changement de la valeur de chaque propriété lié par Binding à la représentation visuelle de l'application XAML, et les procédures de validation par attribut des masque de saisies.
    /// </summary>
    public abstract class ViewModelBase : INotifyPropertyChanged, INotifyDataErrorInfo
    {
        #region INotifyPropertyChanged
        /// <summary>
        /// Evènement et méthodes de fonctionnement des mécanismes de INotifyPropertyChanged.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;
        protected void RaisePropertyChanged<T>(ref T prop, T value, [CallerMemberName] string nomProp = "")
        {
            if ((prop == null) || (!prop.Equals(value)))
            {
                prop = value;
                RaisePropertyChanged(nomProp);
            }
        }
        protected void RaisePropertyChanged(string nomProp)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(nomProp));
            }
        }
        #endregion

        #region INotifyDataErrorInfo
        /// <summary>
        /// Evènement et méthodes de fonctionnement des mécanismes de INotifyDataErrorInfo.
        /// </summary>
        public event EventHandler<DataErrorsChangedEventArgs> ErrorsChanged;
        private readonly ConcurrentDictionary<string, List<ValidationResult>> _errors = new ConcurrentDictionary<string, List<ValidationResult>>();
        public IEnumerable GetErrors(string propertyName)
        {
            List<ValidationResult> propertyErrors = null;
            _errors.TryGetValue(propertyName, out propertyErrors);
            return propertyErrors;
        }
        public bool HasErrors
        {
            get { return _errors.Count > 0; }
        }
        protected virtual void NotifyErrorsChanged([CallerMemberName] String propertyName = "")
        {
            var e = ErrorsChanged;
            if (e == null) return;
            e(this, new DataErrorsChangedEventArgs(propertyName));
        }
        public async void ValidateProperty(string stringValue, [CallerMemberName] String propertyName = "")
        {
            await Task.Factory.StartNew(() =>
            {

                List<ValidationResult> existingErrors;
                _errors.TryRemove(propertyName, out existingErrors);


                var results = new List<ValidationResult>();
                var vc = new ValidationContext(this) { MemberName = propertyName };

                if (Validator.TryValidateProperty(stringValue, vc, results) || results.Count <= 0) return;

                _errors.AddOrUpdate(propertyName, new List<ValidationResult> { new ValidationResult(results[0].ErrorMessage) },
                    (key, existingVal) => new List<ValidationResult> { new ValidationResult(results[0].ErrorMessage) });

                NotifyErrorsChanged(propertyName);
            });
        }
        #endregion
    }
}
