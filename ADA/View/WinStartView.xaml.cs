﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace ADA.View
{
    /// <summary>
    /// Logique d'interaction pour WinStart.xaml
    /// </summary>
    public partial class WinStartView : Window
    {
        public WinStartView()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Méthode de gestion de l'évènement de sortie de l'application appelé par le bouton de sortie et l'appuie sur le bouton "croix rouge" de fermeture de la fenêtre
        /// </summary>
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            MessageBoxResult result = MessageBox.Show("Voulez-vous vraiment quitter cette application ?", "Fermeture de l'application", MessageBoxButton.YesNo, MessageBoxImage.Question);
            if (result == MessageBoxResult.No)
            {
                e.Cancel = true;
            }
        }
    }
}
