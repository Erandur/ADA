﻿using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AdaDal.AdaDto;
using AdaDal.Context;
using AdaDal.Entities;

namespace AdaDal
{
    public class BddAccess : IBddAccess
    {
        public IList<TitreDto> GetAllTitres()
        {
            IList<TitreDto> listeRetour = new List<TitreDto>();
            IEnumerable<TITRE> listeTitre;
            TitreDto nTi;
            using (var _ctx = new ModelBdd())
            {
                //var result = _ctx.TITREs.ToList();
                var result = _ctx.TITREs.ToList();                
                listeTitre = result;
                foreach (TITRE ti in result)
                {
                    nTi = new TitreDto()
                    {
                        LibelleTitre = ti.LIB_TITRE
                    };
                    listeRetour.Add(nTi);
                }
                return listeRetour;
            }
        }
        public TitreDto GetTitreById(int id)
       {
           TitreDto td = null;
           using(var _ctx = new ModelBdd())
           {
               var tdc = _ctx.TITREs.Where( t => t.ID_TITRE == id).FirstOrDefault();
               if(tdc != null)
               {
                    td = new TitreDto { IdTitre = tdc.ID_TITRE, LibelleTitre = tdc.LIB_TITRE };
               }
           }
           return td;
       }

        public string AddTitre(string libelleTitre)
        {
            string ret = string.Empty;
            // Création d'un nouvel objet TitreEntity avec les paramètres de la méthode
            TITRE td = new TITRE()
            {
                LIB_TITRE = libelleTitre
            };

            // Vérification que le titre n'existe pas déjà dans la bdd
            try
            {
                using (var _ctx = new ModelBdd())
                {
                    var test = _ctx.TITREs.Where(t => t.LIB_TITRE == libelleTitre).FirstOrDefault();
                    // Si l'objet n'existe pas dans la base, on l'ajoute
                    if (test != null)
                    {
                        test.LIB_TITRE = td.LIB_TITRE;
                        ret = $" Mise à jour réussie : ID {td.ID_TITRE} - Libelle {td.LIB_TITRE}";
                        _ctx.SaveChanges();
                    }
                    else
                    {
                        _ctx.TITREs.Add(td);
                        _ctx.SaveChanges();
                        ret = $"Enregistrement réussi : ID {td.ID_TITRE} - Libelle {td.LIB_TITRE}";
                    }
                }
            }
            catch (DbException de)
            {
                throw new Exception($"DB ERROR : {de.HResult} - {de.Message} - {de.InnerException}");
            }
            catch(Exception)
            {
                throw;
            }
            return ret;
        }

        public bool DeleteTitre(int id, string libelle)
        {
            bool deleteOk = false;
            using (var _ctx = new ModelBdd())
            {
                TITRE titre = _ctx.TITREs.Where(t => t.ID_TITRE == id && t.LIB_TITRE.ToLower() == libelle.ToLower()).FirstOrDefault();
                _ctx.TITREs.Remove(titre);
                _ctx.SaveChanges();
                deleteOk = true;
            }
            return deleteOk;
        }

        public bool UpdateTitre(int id)
        {
            return false;
        }
    }

}
