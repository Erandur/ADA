﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class AncienneCoteDto
    {
        public DepartementDto Departement { get; set; }
        public FondsDto Fonds { get; set; }
        public char Armoire { get; set; }
        public int Registre { get; set; }
        public int Cahier { get; set; }
        public string Folio { get; set; }
        public IList<ManuscritDto> ListeManuscrit { get; set; }
    }
}
