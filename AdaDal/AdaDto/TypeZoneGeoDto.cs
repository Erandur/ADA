﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class TypeZoneGeoDto
    {
        public string LibTypeZoneGeo { get; set; }
        public IList<ActionDto> ListeAction { get; set; }
    }
}
