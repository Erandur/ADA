﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class IntervenantDto
    {
        public TitreDto Titre { get; set; }
        public string NomInter { get; set; }
        public string PrenomInter { get; set; }
        public string Appellation { get; set; }
        public int Age { get; set; }
        public string Office { get; set; }
        public string ComInter { get; set; }
        public bool Serment { get; set; }
        public IList<ActionDto> ListeTemoins { get; set; }
        public IList<ActionDto> ListeActeur { get; set; }
        public IList<TypeIntervenantDto> ListeTypeInter { get; set; }
    }
}
