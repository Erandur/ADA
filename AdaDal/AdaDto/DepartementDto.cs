﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class DepartementDto
    {
        public string NumDep { get; set; }
        public string LibDep { get; set; }
        public IList<AncienneCoteDto> ListeAncienneCote { get; set; }
    }
}
