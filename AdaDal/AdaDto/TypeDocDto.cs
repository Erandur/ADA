﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class TypeDocDto
    {
        public string LibTypeDoc { get; set; }
        public IList<DocumentDto> ListeDocument { get; set; }
    }
}
