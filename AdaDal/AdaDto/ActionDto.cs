﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class ActionDto
    {
        public IntervenantDto Acteur { get; set; }
        public TypeActionDto TypeAction { get; set; }
        public EnqueteDto Enquete { get; set; }
        public string DescAction { get; set; }
        public DateTime DateDebAction { get; set; }
        public DateTime DateFinAction { get; set; }
        public string RemAction { get; set; }
        public bool MentionCriee { get; set; }
        public IList<IntervenantDto> ListeTemoins { get; set; }
        public IList<DocumentDto> ListeDocuments { get; set; }
        public IList<ZoneGeoHistoDto> ListeZoneGeoHisto { get; set; }

    }
}
