﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class DocumentDto
    {
        public TypeDocDto TypeDocument { get; set; }
        public string Materialite { get; set; }
        public string Emetteur { get; set; }
        public DateTime DateDocument { get; set; }
        public string Valeur { get; set; }
        public string Path { get; set; }
        public IList<ActionDto> ListeAction { get; set; }

    }
}
