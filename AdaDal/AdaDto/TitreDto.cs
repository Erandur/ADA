﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class TitreDto
    {
        public int IdTitre { get; set; }
        public string LibelleTitre { get; set; }
        public IList<IntervenantDto> ListeIntervenant { get; set; }
    }
}
