﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class TypeIntervenantDto
    {
        public string LibInter { get; set; }
        public IList<IntervenantDto> ListeIntervenant { get; set; }
    }
}
