﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class ZoneGeoHistoDto
    {
        public TypeZoneGeoDto TypeZoneGeo { get; set; }
        public ZoneGeoHistoDto ZoneGeoRef { get; set; }
        public string NomZoneGeo { get; set; }
        public string DescZoneGeo { get; set; }
        public IList<ActionDto> ListeAction { get; set; }
        public IList<EnqueteDto>ListeEnqueteRef { get; set; }
    }
}
