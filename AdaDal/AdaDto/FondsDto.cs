﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class FondsDto
    {
        public string LibFonds { get; set; }
        public IList<AncienneCoteDto> ListeAncienneCote { get; set; }
    }
}
