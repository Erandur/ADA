﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class SupportDto
    {
        public string LibSup { get; set; }
        public IList<ManuscritDto> ListeManuscrit { get; set; }
    }
}
