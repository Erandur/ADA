﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class ManuscritDto
    {
        public SupportDto Support { get; set; }
        public string Cote { get; set; }
        public string DepotArchives { get; set; }
        public DateTime DateDebEcriture { get; set; }
        public DateTime DateFinEcriture { get; set; }
        public int NbFolios { get; set; }
        public string RemManuscrit { get; set; }
        public bool Traite { get; set; }
        public bool Lu { get; set; }
        public IList<EnqueteDto> ListeEnquete { get; set; }
        public IList<AncienneCoteDto>ListeAncienneCote { get; set; }
    }
}
