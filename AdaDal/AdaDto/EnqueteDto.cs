﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class EnqueteDto
    {
        public EnqueteDto EnqueteRef { get; set; }
        public TypeEnqueteDto TypeEnquete { get; set; }
        public TypeThemeDto TypeTheme { get; set; }
        public DateTime DateDebEnquete { get; set; }
        public DateTime DateFinEnquete { get; set; }
        public string DescEnquete { get; set; }
        public IList<ManuscritDto> ListeManuscrit { get; set; }
        public IList<ActionDto> ListeAction { get; set; }
        public IList<EnqueteDto> ListeEnqueteLiees { get; set; }


    }
}
