﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal.AdaDto
{
    public class TypeThemeDto
    {
        public string libTypeTheme { get; set; }
        public IList<EnqueteDto> ListeEnquete { get; set; }
    }
}
