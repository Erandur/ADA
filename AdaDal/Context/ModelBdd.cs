namespace AdaDal.Context
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using AdaDal.Entities;

    internal partial class ModelBdd : DbContext
    {
        public ModelBdd()
            : base("name=ModelBdd")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        public virtual DbSet<ACTION> ACTIONs { get; set; }
        public virtual DbSet<ANCIENNE_COTE> ANCIENNE_COTE { get; set; }
        public virtual DbSet<DEPARTEMENT> DEPARTEMENTs { get; set; }
        public virtual DbSet<DOCUMENT> DOCUMENTs { get; set; }
        public virtual DbSet<ENQUETE> ENQUETEs { get; set; }
        public virtual DbSet<FACTORY> FACTORies { get; set; }
        public virtual DbSet<FOND> FONDS { get; set; }
        public virtual DbSet<INTERVENANT> INTERVENANTs { get; set; }
        public virtual DbSet<MANUSCRIT> MANUSCRITs { get; set; }
        public virtual DbSet<SUPPORT> SUPPORTs { get; set; }
        public virtual DbSet<TITRE> TITREs { get; set; }
        public virtual DbSet<TYPE_ACTION> TYPE_ACTION { get; set; }
        public virtual DbSet<TYPE_DOCUMENT> TYPE_DOCUMENT { get; set; }
        public virtual DbSet<TYPE_ENQUETE> TYPE_ENQUETE { get; set; }
        public virtual DbSet<TYPE_INTERVENANT> TYPE_INTERVENANT { get; set; }
        public virtual DbSet<TYPE_THEME> TYPE_THEME { get; set; }
        public virtual DbSet<TYPE_ZONE_GEO> TYPE_ZONE_GEO { get; set; }
        public virtual DbSet<ZONE_GEO_HISTO> ZONE_GEO_HISTO { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACTION>()
                .HasMany(e => e.ZONE_GEO_HISTO)
                .WithMany(e => e.ACTIONs)
                .Map(m => m.ToTable("A_LIEU").MapLeftKey("ID_ACTION").MapRightKey("ID_ZONE_GEO"));

            modelBuilder.Entity<ACTION>()
                .HasMany(e => e.DOCUMENTs)
                .WithMany(e => e.ACTIONs)
                .Map(m => m.ToTable("COMPORTE").MapLeftKey("ID_ACTION").MapRightKey("ID_DOC"));

            modelBuilder.Entity<ACTION>()
                .HasMany(e => e.INTERVENANTs)
                .WithMany(e => e.ACTIONs1)
                .Map(m => m.ToTable("TEMOIN").MapLeftKey("ID_ACTION").MapRightKey("ID_INTER"));

            modelBuilder.Entity<ANCIENNE_COTE>()
                .Property(e => e.ARMOIRE)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<ANCIENNE_COTE>()
                .HasMany(e => e.MANUSCRITs)
                .WithMany(e => e.ANCIENNE_COTE)
                .Map(m => m.ToTable("POSSEDE").MapLeftKey("ID_ANC_COTE").MapRightKey("ID_MANUSCRIT"));

            modelBuilder.Entity<DEPARTEMENT>()
                .Property(e => e.NUM_DEP)
                .IsFixedLength()
                .IsUnicode(false);

            modelBuilder.Entity<DEPARTEMENT>()
                .HasMany(e => e.ANCIENNE_COTE)
                .WithRequired(e => e.DEPARTEMENT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ENQUETE>()
                .HasMany(e => e.ACTIONs)
                .WithRequired(e => e.ENQUETE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ENQUETE>()
                .HasMany(e => e.EnqueteLiees)
                .WithOptional(e => e.EnqueteRef)
                .HasForeignKey(e => e.ID_ENQ_LIE);

            modelBuilder.Entity<ENQUETE>()
                .HasMany(e => e.MANUSCRITs)
                .WithMany(e => e.ENQUETEs)
                .Map(m => m.ToTable("CONTIENT").MapLeftKey("ID_ENQ").MapRightKey("ID_MANUSCRIT"));

            modelBuilder.Entity<FOND>()
                .HasMany(e => e.ANCIENNE_COTE)
                .WithRequired(e => e.FOND)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<INTERVENANT>()
                .HasMany(e => e.ACTIONs)
                .WithRequired(e => e.INTERVENANT)
                .HasForeignKey(e => e.ID_INTER)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<INTERVENANT>()
                .HasMany(e => e.TYPE_INTERVENANT)
                .WithMany(e => e.INTERVENANTs)
                .Map(m => m.ToTable("EST").MapLeftKey("ID_INTER").MapRightKey("ID_TYPE_INTER"));

            modelBuilder.Entity<SUPPORT>()
                .HasMany(e => e.MANUSCRITs)
                .WithRequired(e => e.SUPPORT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TYPE_ACTION>()
                .HasMany(e => e.ACTIONs)
                .WithRequired(e => e.TYPE_ACTION)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TYPE_DOCUMENT>()
                .HasMany(e => e.DOCUMENTs)
                .WithRequired(e => e.TYPE_DOCUMENT)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TYPE_ENQUETE>()
                .HasMany(e => e.ENQUETEs)
                .WithRequired(e => e.TYPE_ENQUETE)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TYPE_THEME>()
                .HasMany(e => e.ENQUETEs)
                .WithRequired(e => e.TYPE_THEME)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TYPE_ZONE_GEO>()
                .HasMany(e => e.ZONE_GEO_HISTO)
                .WithRequired(e => e.TYPE_ZONE_GEO)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ZONE_GEO_HISTO>()
                .HasMany(e => e.ZONE_GEO_LIEES)
                .WithOptional(e => e.ZONE_GEO_REF)
                .HasForeignKey(e => e.ID_ZONE_GEO_LIE);
        }
    }
}
