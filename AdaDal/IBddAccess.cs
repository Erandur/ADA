﻿using AdaDal.AdaDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaDal
{
    public interface IBddAccess
    {
        #region Titre
        // Méthode de récupération de la liste des Titres.
        IList<TitreDto> GetAllTitres ();

        // Méthode de récupération d'un titre par son Id
        TitreDto GetTitreById (int id);

        // Méthode d'ajout d'un Titre
        string AddTitre(string libelleTitre);

        // Méthode de suppression d'un titre
        bool DeleteTitre (int id, string libelle);

        // Méthode de modification d'un Titre
        bool UpdateTitre(int id);
        #endregion
    }
}
