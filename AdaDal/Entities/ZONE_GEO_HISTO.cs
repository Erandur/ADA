namespace AdaDal.Entities
{
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    internal partial class ZONE_GEO_HISTO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ZONE_GEO_HISTO()
        {
            ZONE_GEO_LIEES = new HashSet<ZONE_GEO_HISTO>();
            ACTIONs = new HashSet<ACTION>();
        }

        [Key]
        public int ID_ZONE_GEO { get; set; }

        public int? ID_ZONE_GEO_LIE { get; set; }

        public int ID_TYPE_ZONE { get; set; }

        [Required]
        [StringLength(50)]
        public string NOM_ZONE { get; set; }

        [Required]
        [StringLength(500)]
        public string DESC_ZONE { get; set; }

        public virtual TYPE_ZONE_GEO TYPE_ZONE_GEO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZONE_GEO_HISTO> ZONE_GEO_LIEES { get; set; }

        public virtual ZONE_GEO_HISTO ZONE_GEO_REF { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACTION> ACTIONs { get; set; }
    }
}
