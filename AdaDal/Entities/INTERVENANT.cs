namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("INTERVENANT")]
    internal partial class INTERVENANT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public INTERVENANT()
        {
            ACTIONs = new HashSet<ACTION>();
            TYPE_INTERVENANT = new HashSet<TYPE_INTERVENANT>();
            ACTIONs1 = new HashSet<ACTION>();
        }

        [Key]
        public int ID_INTER { get; set; }

        public int? ID_TITRE { get; set; }

        [StringLength(50)]
        public string NOM { get; set; }

        [StringLength(50)]
        public string PRENOM { get; set; }

        [StringLength(50)]
        public string APPELLATION { get; set; }

        public int? AGE { get; set; }

        [StringLength(50)]
        public string OFFICE { get; set; }

        [Required]
        [StringLength(500)]
        public string COM_INTER { get; set; }

        public bool SERMENT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACTION> ACTIONs { get; set; }

        public virtual TITRE TITRE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<TYPE_INTERVENANT> TYPE_INTERVENANT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACTION> ACTIONs1 { get; set; }
    }
}
