namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SUPPORT")]
    internal partial class SUPPORT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SUPPORT()
        {
            MANUSCRITs = new HashSet<MANUSCRIT>();
        }

        [Key]
        public int ID_SUP { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_SUP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MANUSCRIT> MANUSCRITs { get; set; }
    }
}
