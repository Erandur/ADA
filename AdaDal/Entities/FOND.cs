namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FONDS")]
    internal partial class FOND
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public FOND()
        {
            ANCIENNE_COTE = new HashSet<ANCIENNE_COTE>();
        }

        [Key]
        public int ID_FONDS { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_FONDS { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ANCIENNE_COTE> ANCIENNE_COTE { get; set; }
    }
}
