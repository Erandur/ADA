namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DOCUMENT")]
    internal partial class DOCUMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DOCUMENT()
        {
            ACTIONs = new HashSet<ACTION>();
        }

        [Key]
        public int ID_DOC { get; set; }

        public int ID_TYPE_DOC { get; set; }

        [Required]
        [StringLength(50)]
        public string MATERIALITE { get; set; }

        [Required]
        [StringLength(50)]
        public string EMETTEUR { get; set; }

        [Column(TypeName = "date")]
        public DateTime DATE_DOC { get; set; }

        [Required]
        [StringLength(500)]
        public string VALEUR { get; set; }

        [StringLength(50)]
        public string PATH { get; set; }

        public virtual TYPE_DOCUMENT TYPE_DOCUMENT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACTION> ACTIONs { get; set; }
    }
}
