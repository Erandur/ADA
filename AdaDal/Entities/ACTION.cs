namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ACTION")]
    internal partial class ACTION
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ACTION()
        {
            ZONE_GEO_HISTO = new HashSet<ZONE_GEO_HISTO>();
            DOCUMENTs = new HashSet<DOCUMENT>();
            INTERVENANTs = new HashSet<INTERVENANT>();
        }

        [Key]
        public int ID_ACTION { get; set; }

        public int ID_INTER { get; set; }

        public int ID_TYPE_ACTION { get; set; }

        public int ID_ENQ { get; set; }

        [Required]
        [StringLength(500)]
        public string DESC_ACTION { get; set; }

        [Column(TypeName = "date")]
        public DateTime DEB_ACTION { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FIN_ACTION { get; set; }

        [StringLength(500)]
        public string REM_ACTION { get; set; }

        public bool MENTION_CRIEE { get; set; }

        public virtual ENQUETE ENQUETE { get; set; }

        public virtual INTERVENANT INTERVENANT { get; set; }

        public virtual TYPE_ACTION TYPE_ACTION { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZONE_GEO_HISTO> ZONE_GEO_HISTO { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<DOCUMENT> DOCUMENTs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INTERVENANT> INTERVENANTs { get; set; }
    }
}
