namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    internal partial class TYPE_ZONE_GEO
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TYPE_ZONE_GEO()
        {
            ZONE_GEO_HISTO = new HashSet<ZONE_GEO_HISTO>();
        }

        [Key]
        public int ID_TYPE_ZONE { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_TYPE_ZONE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ZONE_GEO_HISTO> ZONE_GEO_HISTO { get; set; }
    }
}
