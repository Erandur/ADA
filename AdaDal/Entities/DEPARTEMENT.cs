namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("DEPARTEMENT")]
    internal partial class DEPARTEMENT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public DEPARTEMENT()
        {
            ANCIENNE_COTE = new HashSet<ANCIENNE_COTE>();
        }

        [Key]
        public int ID_DEP { get; set; }

        [Required]
        [StringLength(2)]
        public string NUM_DEP { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_DEP { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ANCIENNE_COTE> ANCIENNE_COTE { get; set; }
    }
}
