namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    internal partial class TYPE_INTERVENANT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TYPE_INTERVENANT()
        {
            INTERVENANTs = new HashSet<INTERVENANT>();
        }

        [Key]
        public int ID_TYPE_INTER { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_TYPE_INTER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INTERVENANT> INTERVENANTs { get; set; }
    }
}
