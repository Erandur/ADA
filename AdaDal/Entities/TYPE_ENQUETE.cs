namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    internal partial class TYPE_ENQUETE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TYPE_ENQUETE()
        {
            ENQUETEs = new HashSet<ENQUETE>();
        }

        [Key]
        public int ID_TYPE_ENQ { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_TYPE_ENQ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ENQUETE> ENQUETEs { get; set; }
    }
}
