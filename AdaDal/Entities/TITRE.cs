namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("TITRE")]
    internal partial class TITRE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public TITRE()
        {
            INTERVENANTs = new HashSet<INTERVENANT>();
        }

        [Key]
        public int ID_TITRE { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_TITRE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<INTERVENANT> INTERVENANTs { get; set; }
    }
}
