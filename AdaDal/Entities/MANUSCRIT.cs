namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("MANUSCRIT")]
    internal partial class MANUSCRIT
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public MANUSCRIT()
        {
            ENQUETEs = new HashSet<ENQUETE>();
            ANCIENNE_COTE = new HashSet<ANCIENNE_COTE>();
        }

        [Key]
        public int ID_MANUSCRIT { get; set; }

        public int ID_SUP { get; set; }

        [Required]
        [StringLength(15)]
        public string COTE { get; set; }

        [Required]
        [StringLength(100)]
        public string DEPOT_ARCHIVE { get; set; }

        [Column(TypeName = "date")]
        public DateTime DEB_ECRITURE { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FIN_ECRITURE { get; set; }

        public int NB_FOLIOS { get; set; }

        [Required]
        public string RES_MANUSCRIT { get; set; }

        public bool TRAITE { get; set; }

        public bool LU { get; set; }

        public virtual SUPPORT SUPPORT { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ENQUETE> ENQUETEs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ANCIENNE_COTE> ANCIENNE_COTE { get; set; }
    }
}
