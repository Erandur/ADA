namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ENQUETE")]
    internal partial class ENQUETE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ENQUETE()
        {
            ACTIONs = new HashSet<ACTION>();
            EnqueteLiees = new HashSet<ENQUETE>();
            MANUSCRITs = new HashSet<MANUSCRIT>();
        }

        [Key]
        public int ID_ENQ { get; set; }

        public int? ID_ENQ_LIE { get; set; }

        public int ID_TYPE_ENQ { get; set; }

        public int ID_TYPE_THEME { get; set; }

        [Column(TypeName = "date")]
        public DateTime DEB_ENQ { get; set; }

        [Column(TypeName = "date")]
        public DateTime? FIN_ENQ { get; set; }

        [Required]
        [StringLength(500)]
        public string DESC_ENQ { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ACTION> ACTIONs { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ENQUETE> EnqueteLiees { get; set; }

        public virtual ENQUETE EnqueteRef { get; set; }

        public virtual TYPE_ENQUETE TYPE_ENQUETE { get; set; }

        public virtual TYPE_THEME TYPE_THEME { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MANUSCRIT> MANUSCRITs { get; set; }
    }
}
