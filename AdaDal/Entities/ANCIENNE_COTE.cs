namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    internal partial class ANCIENNE_COTE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public ANCIENNE_COTE()
        {
            MANUSCRITs = new HashSet<MANUSCRIT>();
        }

        [Key]
        public int ID_ANC_COTE { get; set; }

        public int ID_DEP { get; set; }

        public int ID_FONDS { get; set; }

        [Required]
        [StringLength(1)]
        public string ARMOIRE { get; set; }

        public int REGISTRE { get; set; }

        public int CAHIER { get; set; }

        [Required]
        [StringLength(20)]
        public string FOLIO { get; set; }

        public virtual DEPARTEMENT DEPARTEMENT { get; set; }

        public virtual FOND FOND { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MANUSCRIT> MANUSCRITs { get; set; }
    }
}
