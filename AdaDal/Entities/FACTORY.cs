namespace AdaDal.Entities
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("FACTORY")]
    internal partial class FACTORY
    {
        [Key]
        public int ID_FACTORY { get; set; }

        [Required]
        [StringLength(50)]
        public string LIB_FACTORY { get; set; }
    }
}
