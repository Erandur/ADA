﻿using AdaDal;
using AdaDal.AdaDto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AdaConsole
    /// <summary>
    /// Programme console de tests de parsing de documents et d'accès à la base de données pour le Projet Archives Data Analysis
    /// </summary>
{
    class Program
    {
        /// <summary>
        /// Méthode principale d'initialisation du programme.
        /// </summary>
        /// <param name="args"></param>
        private static IBddAccess iAccess;
        static void Main(string[] args)
        {
            // Construction du menu d'intéraction avec l'utilisateur. Modifier ici en fonction de l'ajout de fonctions le nombre maximum d'items du menu.

            int selector = 0;
            int maxMenuItems = 100;
            bool good = false;
            iAccess = new BddAccess();
            while (selector != maxMenuItems)
            {
                Console.Clear();
                DrawTitle();
                DrawMenu(maxMenuItems);
                good = int.TryParse(Console.ReadLine(), out selector);
                if (good)
                {
                    switch (selector)
                    {
                        // Première partie : Gestion des documents à parser
                        case 1:
                            Console.WriteLine("On envoie le document dans la factory.");
                            break;

                        // Deuxième partie : Gestion des enquêtes et des procédure
                        // Création d'une enquête
                        case 2:
                            Console.WriteLine("On crée une enquête.");
                            break;
                        // Création d'une procédure
                        case 3:
                            Console.WriteLine("On crée une procédure.");
                            break;
                        // Création d'un intervenant
                        case 4:
                            Console.WriteLine("On crée un intervenant.");
                            break;
                        // Création d'une zone géographique
                        case 5:
                            Console.WriteLine("On crée une zone géographique.");
                            break;
                        // Création d'une pièce supplémentaire
                        case 6:
                            Console.WriteLine("On crée une pièce supplémentaire.");
                            break;
                        // Ajout d'une procédure à une enquête
                        case 7:
                            Console.WriteLine("On ajoute une procédure à une enquête.");
                            break;
                        // Ajout d'un intervenant à une procédure
                        case 8:
                            Console.WriteLine("On ajoute un intervenant à une enquête.");
                            break;
                        // Ajout d'une zone géographique à une procédure
                        case 9:
                            Console.WriteLine("On ajoute une zone géographique à une procédure.");
                            break;
                        // Ajout d'une pièce supplémentaire à une procédure
                        case 10:
                            Console.WriteLine("On ajoute une pièce supplémentaire à une procédure.");
                            break;
                        // Mise en relation d'une enquête par rapport à une autre si besoin
                        case 11:
                            Console.WriteLine("On lie une enquête précédente à l'enquête en cours.");
                            break;

                        // Troisième partie : Affichage des données
                        // Affichage de toutes les enquêtes
                        case 12:
                            Console.WriteLine("On affiche toutes les enquêtes.");
                            break;
                        // Affichage des procédures d'une enquête spécifique
                        case 13:
                            Console.WriteLine("On affiche toutes les procédures d'une enquête.");
                            break;
                        // Affichage de toutes les enquêtes d'une zone géographique spécifique
                        case 14:
                            Console.WriteLine("On affiche toutes les enquêtes d'une zone géographique.");
                            break;
                        // Affichage de toutes les enquêtes d'un commanditaire spécifique
                        case 15:
                            Console.WriteLine("On affiche toutes les enquêtes d'un commanditaire.");
                            break;
                        // Affichage de tous les intervenants d'une enquête spécifique
                        case 16:
                            Console.WriteLine("On affiche tous les intervenants d'une enquête.");
                            break;
                        // Affichage de toutes les enquêtes d'un type particulier
                        case 17:
                            Console.WriteLine("On affiche toutes les enquêtes d'un type.");
                            break;
                        // Affichage de toutes les enquêtes d'un thème particulier
                        case 18:
                            Console.WriteLine("On affiche toutes les enquêtes d'un thème.");
                            break;

                        // Quatrième partie : Tests 
                        // Affichage de tous les titres
                        case 19:
                            IList<TitreDto> listeTitre = GetTitres();
                            foreach (TitreDto Ti in listeTitre)
                            {
                                Console.WriteLine(Ti.LibelleTitre);
                            }
                            break;
                        // Affichage d'un titre par son id
                        case 20:
                            AfficherTitre();
                            break;
                        // Ajout d'un nouveau titre dans la bdd
                        case 21:
                            CreationTitre();
                            break;
                        // Suppression d'un titre dans la bdd
                        case 22:
                            SuppressionTitre();
                            break;
                        case 99:
                            FermetureApplication();
                            break;
                        default:
                            if (selector != maxMenuItems)
                            {
                                Console.WriteLine("Erreur message");
                            }
                            break;
                    }
                }
                else
                {
                    Console.WriteLine("Erreur message");
                }
                Console.ReadKey();
            }
        }

        /// <summary>
        /// Méthode de création d'une liste des titres existants dans la bdd.
        /// </summary>
        /// <returns>Une liste des titres enregistrés dans la bdd</returns>
        static IList<TitreDto> GetTitres()
        {
            return iAccess.GetAllTitres();
        }

        static TitreDto GetTitreById (int id)
        {
            return iAccess.GetTitreById(id);
        }



        static void CreationTitre ()
        {            
            string reponse = string.Empty;

            Console.WriteLine("Entrez le libellé du titre :");
            reponse = Console.ReadLine().ToUpper();
            try
            {                
                Console.WriteLine(iAccess.AddTitre(reponse));
            }
            catch (Exception ex)
            {
                Console.WriteLine($"ERROR : {ex.HResult} - {ex.Message}");
            }            
        }
        
        static void SuppressionTitre()
        {
            int id;
            bool deleteOk = false;

            Console.WriteLine("Entrez l'id du titre à supprimer :");
            int.TryParse(Console.ReadLine(), out id);
            var titre = iAccess.GetTitreById(id);

            if (titre != null)
            {
                deleteOk = iAccess.DeleteTitre(titre.IdTitre, titre.LibelleTitre); 
            }
            if (deleteOk)
                Console.WriteLine("Suppression effecué");
            else
                Console.WriteLine("Echec de la suppression");
        }

        private static void AfficherTitre()
        {
            bool goodId = false;
            bool nouvelleSaisie = true;
            int id;
            TitreDto td;

            Console.WriteLine("Selectionnez l'id du titre à afficher : ");
            goodId = int.TryParse(Console.ReadLine(), out id);
            // Vérification de la saisie
            // On boucle tant que l'utilisateur n'a pas saisi un nombre
            while (!goodId)
            {
                Console.WriteLine("Erreur de saisie ! Veuillez saisir un nombre !");
                goodId = int.TryParse(Console.ReadLine(), out id);
            }
            td = GetTitreById(id);
            if (td != null)
                nouvelleSaisie = false;

            while (nouvelleSaisie)
            {
                Console.WriteLine("Enregistrement inexistant. Souhaitez-vous effectuer une nouvelle recherche (O/N) ?");
                if (Console.ReadLine().ToLowerInvariant() == "o")
                {
                    Console.WriteLine("Selectionnez l'id du titre à afficher : ");
                    goodId = int.TryParse(Console.ReadLine(), out id);
                    if (goodId)
                        td = GetTitreById(id);
                    else
                        nouvelleSaisie = true;
                }
                else
                    Console.WriteLine("Retour au menu."); nouvelleSaisie = false;
                
            }
            if (td != null)
            {
                Console.WriteLine(td.LibelleTitre);
            }
        }

        private static void DrawStarLine()
        {
            Console.WriteLine("**********************************************************");
        }
        private static void DrawTitle()
        {
            DrawStarLine();
            Console.WriteLine("++++++++++   Analyses des Données d'Archives   +++++++++++");
            DrawStarLine();
        }
        private static void DrawMenu(int maxitems)
        {
            DrawStarLine();
            Console.WriteLine("************************   MENU   ************************");
            Console.WriteLine("Choisissez un nombre :");
            // 1° Partie du menu, gestion des enregistements et parsing des docs.    
            Console.WriteLine("1. Enregistrer un document.");

            // 2° Partie du menu, gestion des enquêtes et des procédures.    
            Console.WriteLine("2. Créer une Enquête.");
            Console.WriteLine("3. Créer une Procédure.");
            Console.WriteLine("4. Créer un Intervenant.");
            Console.WriteLine("5. Créer une Zone Géographique.");
            Console.WriteLine("6. Créer une pièce supplémentaire.");
            Console.WriteLine("7. Ajouter une procédure à une enquête.");
            Console.WriteLine("8. Ajouter un intervenant à une procédure.");
            Console.WriteLine("9. Ajouter une zone géographique à une procédure.");
            Console.WriteLine("10. Ajouter une pièce supplémentaire à une procédure.");
            Console.WriteLine("11. Lier une précédente enquête à une enquête en cours.");

            // 3° Partie du menu, affichages des données.
            Console.WriteLine("12. Afficher toutes les enquêtes.");
            Console.WriteLine("13. Afficher toutes les procédures d'une enquêtes.");
            Console.WriteLine("14. Afficher toutes les enquêtes d'une zone géographique.");
            Console.WriteLine("15. Afficher toutes les enquêtes d'un commenditaire.");
            Console.WriteLine("16. Afficher tous les intervenants d'une enquête.");
            Console.WriteLine("17. Afficher toutes les enquêtes d'un type.");
            Console.WriteLine("18. Afficher toutes les enquêtes d'un thème.");

            // 4° Partie Tests
            Console.WriteLine("19. Afficher tous les titres.");
            Console.WriteLine("20. Afficher un titre par rapport à son id.");
            Console.WriteLine("21. Ajouter un titre.");
            Console.WriteLine("22. Supprimer un titre.");

            // 5° Partie du menu, sortie de l'application.
            Console.WriteLine("99. Quitter.");
            DrawStarLine();
            Console.WriteLine("Choissez une fonction: tapez 1, 2,... ou {0} pour quitter.", maxitems - 1);
            DrawStarLine();
        }

        static void FermetureApplication()
        {
            Console.WriteLine("Fermeture de l'application.");
            System.Threading.Thread.Sleep(500);
            Environment.Exit(0);
        }
    }
}
